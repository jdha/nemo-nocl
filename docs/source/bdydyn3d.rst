
AMM12 tests with various 3d boundary conditions
===============================================

Eight tests were run to examine the impact of activating 3d dynamical
boundary conditions following Momme's concerns about nutrients being
advected in the vertical close to the open boundary. Code for
zero-gradient tangential velocities and the Neumann condition was
written into a branch of v3.6
(https://forge.ipsl.jussieu.fr/nemo/browser/branches/NERC/dev\_r5549\_BDY\_ZEROGRAD).

The first four tests use relaxtion for the tracers, with the 3d
dynamical boundary either absent (none), zero-gradient tangential
velocities (zg), zg with an update for the gradient in kenetic energy
(ke) on the open boundary or Neumann boundary condtion (neu) with ke.
The second set of four tests are the same as the first except the tracer
boundary condtion was set to the Neumann condtion. This was used to
replicate what might be happening to the nutrients close to the boundary
- as the current use of the relaxtion condtion may just be masking
dynamical problems close to the open boundary.

+--------+-------------+-----------+
| Test   | cn\_dyn3d   | cn\_tra   |
+========+=============+===========+
| 01     | none        | frs       |
+--------+-------------+-----------+
| 02     | zg          | frs       |
+--------+-------------+-----------+
| 03     | zg + ke     | frs       |
+--------+-------------+-----------+
| 04     | neu + ke    | frs       |
+--------+-------------+-----------+
| 05     | none        | neu       |
+--------+-------------+-----------+
| 06     | zg          | neu       |
+--------+-------------+-----------+
| 07     | zg + ke     | neu       |
+--------+-------------+-----------+
| 08     | neu + ke    | neu       |
+--------+-------------+-----------+

**NB Test 05 only got to time step 44 before crashing**

.. code:: python

    import numpy as np
    import matplotlib.pyplot as plt
    from netCDF4 import Dataset
    from mpl_toolkits.basemap import Basemap
    import matplotlib.cm as cm
    import seaborn as sns
    
    %matplotlib inline

.. code:: python

    cmap=sns.choose_colorbrewer_palette('diverging',as_cmap=True)



.. image:: bdydyn3d_images/Untitled_3_0.png


.. code:: python

    def quick_plt(varnam,fname,cmin,cmax):
    
        f, ax = plt.subplots(nrows=4, ncols=2, figsize=(18, 16))
    
        dirname = '/Users/jdha/Desktop/AMM12_tests/'
        filname = 'bathy_meter.nc'
        rootbat = Dataset(dirname+filname, "r", format="NETCDF4")
        depths = rootbat.variables['Bathymetry']
    
        dirname = '/Users/jdha/Desktop/AMM12_tests/'
        filname = 'mesh_mask.nc'
        rootmsh = Dataset(dirname+filname, "r", format="NETCDF4")
      
        zt      = np.squeeze(rootmsh.variables['gdept_0'][0,:,:,4])
        lat     = np.squeeze(rootmsh.variables['gphiu'][0,:,4])
        msk     = np.squeeze(rootmsh.variables['umask'][0,:,:,4])
        rootmsh.close()
    
        for l in range(8):
            plt.sca(ax[np.mod(l,4),l//4])
            filname = fname+'_0'+str(l+1)+'.nc'
            rootgrp = Dataset(dirname+filname, "r", format="NETCDF4")
            un      = np.squeeze(rootgrp.variables[varnam][0,:,:,4])
            un[un==0] = np.NaN
            rootgrp.close()
            plt.pcolormesh(np.tile(lat[:,np.newaxis],(1,51)).T,zt,un,cmap=cmap)
            plt.clim((cmin,cmax))
            plt.gca().invert_yaxis()
            cb = plt.colorbar()
            plt.gca().set_axis_bgcolor((0.5, 0.5, 0.5))
            plt.title((varnam+' for test:0'+str(l+1)))
        
        rootbat.close()
        
    


.. code:: python

    fname = 'AMM12_00001296_restart_oce_out'
    quick_plt('tn',fname,6,14)



.. image:: bdydyn3d_images/Untitled_5_0.png


.. code:: python

    quick_plt('un',fname,-0.4,0.4)



.. image:: bdydyn3d_images/Untitled_6_0.png


.. code:: python

    quick_plt('vn',fname,-0.4,0.4)



.. image:: bdydyn3d_images/Untitled_7_0.png


.. code:: python

    fname = 'W'
    quick_plt('vovecrtz',fname,-0.005,0.005)



.. image:: bdydyn3d_images/Untitled_8_0.png


